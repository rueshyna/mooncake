# Api
For all requests by default, it will return the latest 10 records(`n=10` and `p=0`). Using the `n=` to get more records but it's upto `50`. The `p=` can used to control paginations.


## Api `v0` to `v1`
| v0 | v1| note |
|---|---|---|
| v0/blocks                      | v1/blocks | |
| v0/block/{hash}                | v1/blocks/{hash\|level} | |
| -                              | v1/blocks_num | |
| v0/operations                  | v1/operations |`v1/transactions?op={op_hash}` for more detail, same for `originations` `delegations` `endorsements` |
| v0/operation/{hash}            | v1/operations/{hash}| |
| -                              | v1/operations_num | |
| v0/block_operation             | v1/transactions?block={hash\|level} and others (see note) | others: `originations` `delegations` `endorsements` |
| -                              | v1/transactions_num?block={hash\|level} and others (see note) | others: `originations` `delegations` `endorsements`|
| v0/account_transactions        | v1/transactions?account={account} | same for `originations` `delegations` `endorsements`|
| -                              | v1/transactions_num?account={account} | same for `originations` `delegations` `endorsements`|
| v0/operations?type=transaction | v1/transactions | same for `originations` `delegations` `endorsements`|
| -                              | v1/transactions_num | same for `originations` `delegations` `endorsements`|
| v0/protocals | v1/protocals | |
|              | v1/protocals_num | |

## /v1/blocks
### GET 200

### Query
- p: page
- n: number per page

## /v1/blocks/\<hash\>|\<level\>
### GET 200

## /v1/blocks_num
Return the total number of blocks
### GET 200

## /v1/operations
### GET 200

### Query
- p: page
- n: number per page

## /v1/operations/\<op_hash\>
### GET 200

## /v1/operations_num
Return the total number of operations
### GET 200

## /v1/transactions
### GET 200

### Query
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/transactions_num
Return the total number of transactions
### GET 200

### Query
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/endorsements
### GET 200

### Query
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/endorsements_num
Return the total number of transactions
### GET 200

### Query
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/delegations
### GET 200

### Query
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/delegations_num
Return the total number of transactions
### GET 200

### Query
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/originations
### GET 200

### Query
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/originations_num
Return the total number of transactions
### GET 200

### Query
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/ballots
### GET 200

### Query
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`
- proposal: filter by proposal.

## /v1/ballots_num
Return the total number of transactions
### GET 200

### Query
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`
- proposal: filter by proposal.

## /v1/proposals
### GET 200

### Query
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/proposals_num
Return the total number of proposals
### GET 200

### Query
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/proposers
### GET 200

### Query
- p: page
- n: number per page

## /v1/proposers_num
Return the total number of proposers
### GET 200

## /v1/activate_accounts
### GET 200

### Query
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/activate_accounts_num
Return the total number of proposers
### GET 200

- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/reveals
### GET 200

### Query
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/reveals_num
Return the total number of proposers
### GET 200

- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/seed_nonce_revelations
### GET 200

### Query
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash

## /v1/seed_nonce_revelations_num
Return the total number of seed_nonce_revelations
### GET 200

- block: can be filtered `level` or `hash` of block
- op: filter by op_hash

## /v1/protocals
### GET 200

### Query
- p: page
- n: number per page

## /v1/protocals
Return the total number of protocals
### GET 200

## /v1/double_baking_evidences
### GET 200

### Query
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash

## /v1/double_baking_evidences
Return the total number of double_baking_evidences
### GET 200

- block: can be filtered `level` or `hash` of block
- op: filter by op_hash

## /v1/double_endorsement_evidences
### GET 200

### Query
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash

## /v1/double_endorsement_evidences_num
Return the total number of endorsement
### GET 200

- block: can be filtered `level` or `hash` of block
- op: filter by op_hash

## /v1/24_hours_stat
Return the total number of double_endorsement_evidences
### GET 200

- block: can be filtered `level` or `hash` of block
- op: filter by op_hash

## /v1/accounts
### GET 200

### Query
- p: page
- n: number per page
- prefix: can be filtered by prerix, for example: `tz`, `tz1`, `tz2` ,`KT1`, etc.

## /v1/accounts_num
Return the total number of accounts
### GET 200
- prefix: can be filtered by prerix, for example: `tz`, `tz1`, `tz2` ,`KT1`, etc.

