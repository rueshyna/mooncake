#!/bin/bash


docker rm mysql_db -f

docker run -p 3306:3306 --rm --name mysql_db -e MYSQL_ROOT_PASSWORD=pw -d mariadb:10.3

sleep 10

docker exec -i mysql_db mysql -uroot -ppw < ./test_data.sql
