{-# LANGUAGE OverloadedStrings #-}
--
module Main where
--
import Test.Tasty
import Test.Tasty.HUnit
--
import Mooncake.Teznames
import Mooncake.Teznames.Crypto
import Mooncake.Teznames.RPC
--
import Data.Text as T
--
main :: IO ()
main = do
  defaultMain (testGroup "Our Library Tests" [])
--
