#!/bin/bash

ENTRY=${1:-"localhost:8080"}

test_mooncake() {
  echo "========================================="
  echo "Testing entry ... $1"
  curl -v -f -S ${ENTRY}/$1 > /dev/null
  RESULT=$?
  if [ ${RESULT} -eq 0 ]
    then
    echo "... DONE"
    echo ""
    echo ""
  else
    echo "... FAILED"
    exit ${RESULT}
  fi
}

test_mooncake "v1/blocks"
test_mooncake "v1/blocks/0"
test_mooncake "v1/blocks_num"

test_mooncake "v1/operations"
test_mooncake "v1/operations/abc"
test_mooncake "v1/operations_num"

test_mooncake "v1/transactions"
test_mooncake "v1/transactions?account=test"
test_mooncake "v1/transactions?block=test"
test_mooncake "v1/transactions?op=test"
test_mooncake "v1/transactions_num?account=test"
test_mooncake "v1/transactions_num?block=test"
test_mooncake "v1/transactions_num?op=test"
test_mooncake "v1/transactions_num"

test_mooncake "v1/endorsements?account=test"
test_mooncake "v1/endorsements?block=test"
test_mooncake "v1/endorsements?op=test"
test_mooncake "v1/endorsements"
test_mooncake "v1/endorsements_num?account=test"
test_mooncake "v1/endorsements_num?block=test"
test_mooncake "v1/endorsements_num?op=test"
test_mooncake "v1/endorsements_num"

test_mooncake "v1/delegations?account=test"
test_mooncake "v1/delegations?block=test"
test_mooncake "v1/delegations?op=test"
test_mooncake "v1/delegations"
test_mooncake "v1/delegations_num?account=test"
test_mooncake "v1/delegations_num?block=test"
test_mooncake "v1/delegations_num?op=test"
test_mooncake "v1/delegations_num"

test_mooncake "v1/originations?account=test"
test_mooncake "v1/originations?block=test"
test_mooncake "v1/originations?op=test"
test_mooncake "v1/originations"
test_mooncake "v1/originations_num?account=test"
test_mooncake "v1/originations_num?block=test"
test_mooncake "v1/originations_num?op=test"
test_mooncake "v1/originations_num"

test_mooncake "v1/ballots?account=test"
test_mooncake "v1/ballots?block=test"
test_mooncake "v1/ballots?op=test"
test_mooncake "v1/ballots?proposal=test"
test_mooncake "v1/ballots"
test_mooncake "v1/ballots_num?account=test"
test_mooncake "v1/ballots_num?block=test"
test_mooncake "v1/ballots_num?op=test"
test_mooncake "v1/ballots_num?proposal=test"
test_mooncake "v1/ballots_num"

test_mooncake "v1/proposals?account=test"
test_mooncake "v1/proposals?block=test"
test_mooncake "v1/proposals?op=test"
test_mooncake "v1/proposals"
test_mooncake "v1/proposals_num?account=test"
test_mooncake "v1/proposals_num?block=test"
test_mooncake "v1/proposals_num?op=test"
test_mooncake "v1/proposals_num"

test_mooncake "v1/proposers"
test_mooncake "v1/proposers_num"

test_mooncake "v1/activate_accounts?account=test"
test_mooncake "v1/activate_accounts?block=test"
test_mooncake "v1/activate_accounts?op=test"
test_mooncake "v1/activate_accounts"
test_mooncake "v1/activate_accounts_num?account=test"
test_mooncake "v1/activate_accounts_num?block=test"
test_mooncake "v1/activate_accounts_num?op=test"
test_mooncake "v1/activate_accounts_num"

test_mooncake "v1/reveals?account=test"
test_mooncake "v1/reveals?block=test"
test_mooncake "v1/reveals?op=test"
test_mooncake "v1/reveals"
test_mooncake "v1/reveals_num?account=test"
test_mooncake "v1/reveals_num?block=test"
test_mooncake "v1/reveals_num?op=test"
test_mooncake "v1/reveals_num"

test_mooncake "v1/seed_nonce_revelations?block=test"
test_mooncake "v1/seed_nonce_revelations?op=test"
test_mooncake "v1/seed_nonce_revelations"
test_mooncake "v1/seed_nonce_revelations_num?block=test"
test_mooncake "v1/seed_nonce_revelations_num?op=test"
test_mooncake "v1/seed_nonce_revelations_num"

test_mooncake "v1/protocals"
test_mooncake "v1/protocals_num"

test_mooncake "v1/double_baking_evidences?block=test"
test_mooncake "v1/double_baking_evidences?op=test"
test_mooncake "v1/double_baking_evidences"
test_mooncake "v1/double_baking_evidences_num?block=test"
test_mooncake "v1/double_baking_evidences_num?op=test"
test_mooncake "v1/double_baking_evidences_num"

test_mooncake "v1/double_endorsement_evidences?block=test"
test_mooncake "v1/double_endorsement_evidences?op=test"
test_mooncake "v1/double_endorsement_evidences"
test_mooncake "v1/double_endorsement_evidences_num?block=test"
test_mooncake "v1/double_endorsement_evidences_num?op=test"
test_mooncake "v1/double_endorsement_evidences_num"

test_mooncake "v1/accounts?prefix=test"
test_mooncake "v1/accounts"
test_mooncake "v1/accounts_num?prefix=test"
test_mooncake "v1/accounts_num"

test_mooncake "v1/24_hours_stat"
