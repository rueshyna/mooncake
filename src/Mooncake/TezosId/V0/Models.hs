{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Mooncake.TezosId.V0.Models where


import Database.Persist.TH
import Data.Time.Clock

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Block json
  Id Int sql=uid
  hash String
  operations Int
  protocol String
  net_id String
  level Int
  proto String
  predecessor String
  timestamp String
  validation_pass String
  operations_hash String
  fitness String
  metadata String sql=data
  unix Int
  insert_timestamp UTCTime Maybe default=now()
  UniqueHash hash
  deriving Show

Baker json
  Id Int sql=uid
  blockId BlockId sql=block_uid
  hash String
  predecessor String
  level Int
  priority Int
  seed_nonce_hash String
  proof_of_work_nonce String
  baker String
  insert_timestamp UTCTime Maybe default=now()
  UniqueBakerHash hash
  deriving Show

Operation json
  Id Int sql=uid
  blockId BlockId sql=block_uid
  hash String
  kind String
  block String
  slot String
  level String
  nonce String
  chainId String sql=id
  amount String
  destination String
  managerPubkey String sql=managerPubkey
  balance String
  spendable String
  delegatable String
  script String
  parameters String
  timestamp String
  insert_timestamp UTCTime Maybe default=now()
  unix Int
  branch String
  source String
  public_key String
  fee String
  counter String
  delegate String
  signature String
  inblock String
  UniqueOperation blockId hash kind destination source counter amount
  deriving Show

Peerid json
  Id Int sql=uid
  peerid String
  score String
  trusted String
  state String
  reachable_addr String
  reachable_ip String
  reachable_port String
  total_sent Int
  total_recv Int
  current_inflow Int
  current_outflow Int
  rejected_addr String
  rejected_ip String
  rejected_port String
  rejected_time String
  rejected_unix String
  established_addr String
  established_ip String
  established_port String
  established_time String
  established_unix String
  disconnection_addr String
  disconnection_ip String
  disconnection_port String
  disconnection_time String
  disconnection_unix String
  seen_addr String
  seen_ip String
  seen_port String
  seen_time String
  seen_unix String
  miss_addr String
  miss_ip String
  miss_port String
  miss_time String
  miss_unix String
  timestamp Int
  insert_timestamp UTCTime Maybe default=now()
  countrycode String
  deriving Show
|]
