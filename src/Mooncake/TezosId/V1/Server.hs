{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE TypeOperators              #-}

module Mooncake.TezosId.V1.Server
  ( APIV1
  , serverV1)
  where

import Mooncake.TezosId.V1.AppBS
import Mooncake.TezosId.V1.DBModels
import Mooncake.Common.Config

import Data.Maybe
import Data.Pool

import Control.Monad.IO.Class  (liftIO)

import Servant

import qualified Database.Groundhog.Postgresql as G


type APIV1 =
  "v1" :> "blocks"
       :> QueryParam "p" Int
       :> QueryParam "n" Int
       :> Get '[JSON] [ BlockDetail ]
  :<|> "v1" :> "blocks"
            :> Capture "q" String
            :> Get '[JSON] (Maybe BlockDetail)
  :<|> "v1" :> "blocks_num"
            :> Get '[JSON] Int
  :<|> "v1" :> "operations"
            :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> Get '[JSON] [ Ops ]
  :<|> "v1" :> "operations"
            :> Capture "hash" String
            :> Get '[JSON] (Maybe Ops)
  :<|> "v1" :> "operations_num"
            :> Get '[JSON] Int
  :<|> "v1" :> "transactions"
            :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] [ OpTxsDetail ]
  :<|> "v1" :> "transactions_num"
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] Int
  :<|> "v1" :> "endorsements"
            :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] [ OpEndorsDetail ]
  :<|> "v1" :> "endorsements_num"
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] Int
  :<|> "v1" :> "delegations"
            :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] [ OpDelegationsDetail ]
  :<|> "v1" :> "delegations_num"
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] Int
  :<|> "v1" :> "originations"
            :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] [ OpOriginationsDetail ]
  :<|> "v1" :> "originations_num"
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] Int
  :<|> "v1" :> "ballots"
            :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> QueryParam "proposal" String
            :> Get '[JSON] [ OpBallotsDetail ]
  :<|> "v1" :> "ballots_num" :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> QueryParam "proposal" String
            :> Get '[JSON] Int
  :<|> "v1" :> "proposals" :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] [ OpProposalsDetail ]
  :<|> "v1" :> "proposals_num"
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] Int
  :<|> "v1" :> "proposers" :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> Get '[JSON] [ Proposers ]
  :<|> "v1" :> "proposers_num"
            :> Get '[JSON] Int
  :<|> "v1" :> "activate_accounts"
            :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] [ OpActivateAccountsDetail ]
  :<|> "v1" :> "activate_accounts_num"
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] Int
  :<|> "v1" :> "reveals"
            :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] [ OpRevealsDetail ]
  :<|> "v1" :> "reveals_num"
            :> QueryParam "account" String
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] Int
  :<|> "v1" :> "seed_nonce_revelations"
            :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] [ OpSeedNonceRevelationsDetail ]
  :<|> "v1" :> "seed_nonce_revelations_num"
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] Int
  :<|> "v1" :> "protocols" :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> Get '[JSON] [ Protocols ]
  :<|> "v1" :> "protocols_num"
            :> Get '[JSON] Int
  :<|> "v1" :> "double_baking_evidences"
            :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] [ OpDoubleBakingEvidencesDetail ]
  :<|> "v1" :> "double_baking_evidences_num"
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] Int
  :<|> "v1" :> "double_endorsement_evidences"
            :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] [ OpDoubleEndorsementEvidencesDetail ]
  :<|> "v1" :> "double_endorsement_evidences_num"
            :> QueryParam "block" String
            :> QueryParam "op" String
            :> Get '[JSON] Int
  :<|> "v1" :> "accounts"
            :> QueryParam "p" Int
            :> QueryParam "n" Int
            :> QueryParam "prefix" String
            :> Get '[JSON] [Accounts]
  :<|> "v1" :> "accounts_num"
            :> QueryParam "prefix" String
            :> Get '[JSON] Int
  :<|> "v1" :> "24_hours_stat"
            :> Get '[JSON] Stat
  :<|> "v1" :> "circulating"
            :> Get '[JSON] (Maybe TulipToolsCirculating)
  :<|> "v1" :> "total"
            :> Get '[JSON] (Maybe TulipToolsTotal)

serverV1 :: Pool G.Postgresql -> Server APIV1
serverV1 pp = blocksV1 pp
             :<|> blockV1 pp
             :<|> blocksNumV1 pp
             :<|> opsV1 pp
             :<|> opV1 pp
             :<|> opsNumV1 pp
             :<|> txsV1 pp
             :<|> txsNumV1 pp
             :<|> endorsV1 pp
             :<|> endorsNumV1 pp
             :<|> delegationsV1 pp
             :<|> delegationsNumV1 pp
             :<|> originationsV1 pp
             :<|> originationsNumV1 pp
             :<|> ballotsV1 pp
             :<|> ballotsNumV1 pp
             :<|> proposalsV1 pp
             :<|> proposalsNumV1 pp
             :<|> proposersV1 pp
             :<|> proposersNumV1 pp
             :<|> activateAccountsV1 pp
             :<|> activateAccountsNumV1 pp
             :<|> revealsV1 pp
             :<|> revealsNumV1 pp
             :<|> seedNonceEvelationsV1 pp
             :<|> seedNonceEvelationsNumV1 pp
             :<|> protocolsV1 pp
             :<|> protocolsNumV1 pp
             :<|> doubleBakingEvidencesV1 pp
             :<|> doubleBakingEvidencesNumV1 pp
             :<|> doubleEndorsementEvidencesV1 pp
             :<|> doubleEndorsementEvidencesNumV1 pp
             :<|> accountsV1 pp
             :<|> accountsNumV1 pp
             :<|> twentyFourStatV1 pp
             :<|> tulipToolsCirculating pp
             :<|> tulipToolsTotal pp

tulipToolsTotal :: Pool G.Postgresql
         -> Handler (Maybe TulipToolsTotal)
tulipToolsTotal p = liftIO $ tulipToolsTotal' p

tulipToolsCirculating :: Pool G.Postgresql
         -> Handler (Maybe TulipToolsCirculating)
tulipToolsCirculating p = liftIO $ tulipToolsCirculating' p

blocksV1 :: Pool G.Postgresql
         -> Maybe Int     -- ^ page
         -> Maybe Int     -- ^ number
         -> Handler [ BlockDetail ]
blocksV1 p mpg mnu = liftIO $ latestBlocks' p pg nu
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

blocksNumV1 :: Pool G.Postgresql
            -> Handler Int
blocksNumV1 p = liftIO $ blocksNum p

blockV1 :: Pool G.Postgresql
        -> String
        -> Handler (Maybe BlockDetail)
blockV1 p q = liftIO $ blockDetail p q

opsV1 :: Pool G.Postgresql
            -> Maybe Int     -- ^ page
            -> Maybe Int     -- ^ number
            -> Handler [ Ops ]
opsV1 p mpg mnu = liftIO $ lastesOps p pg nu
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

opsNumV1 :: Pool G.Postgresql
            -> Handler Int
opsNumV1 p = liftIO $ opsNum p

opV1 :: Pool G.Postgresql
        -> String
        -> Handler (Maybe Ops)
opV1 p q = liftIO $ opDetail p q

txsV1 :: Pool G.Postgresql
       -> Maybe Int     -- ^ page
       -> Maybe Int     -- ^ number
       -> Maybe String  -- ^ account
       -> Maybe String  -- ^ block
       -> Maybe String  -- ^ op
       -> Handler [ OpTxsDetail ]
txsV1 p mpg mnu ma mb mo = liftIO $ lastesTxs p pg nu ma mb mo
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

txsNumV1 :: Pool G.Postgresql
         -> Maybe String  -- ^ account
         -> Maybe String  -- ^ block
         -> Maybe String  -- ^ op
         -> Handler Int
txsNumV1 p a b o = liftIO $ txsNum p a b o

endorsV1 :: Pool G.Postgresql
       -> Maybe Int     -- ^ page
       -> Maybe Int     -- ^ number
       -> Maybe String  -- ^ account
       -> Maybe String  -- ^ block
       -> Maybe String  -- ^ op
       -> Handler [ OpEndorsDetail ]
endorsV1 p mpg mnu ma mb mo = liftIO $ lastesEndors p pg nu ma mb mo
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

endorsNumV1 :: Pool G.Postgresql
         -> Maybe String  -- ^ account
         -> Maybe String  -- ^ block
         -> Maybe String  -- ^ op
         -> Handler Int
endorsNumV1 p a b o = liftIO $ endorsNum p a b o

delegationsV1 :: Pool G.Postgresql
       -> Maybe Int     -- ^ page
       -> Maybe Int     -- ^ number
       -> Maybe String  -- ^ account
       -> Maybe String  -- ^ block
       -> Maybe String  -- ^ op
       -> Handler [ OpDelegationsDetail ]
delegationsV1 p mpg mnu ma mb mo = liftIO $ lastesDelegations p pg nu ma mb mo
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

delegationsNumV1 :: Pool G.Postgresql
         -> Maybe String  -- ^ account
         -> Maybe String  -- ^ block
         -> Maybe String  -- ^ op
         -> Handler Int
delegationsNumV1 p a b o = liftIO $ delegationsNum p a b o

originationsV1 :: Pool G.Postgresql
       -> Maybe Int     -- ^ page
       -> Maybe Int     -- ^ number
       -> Maybe String  -- ^ account
       -> Maybe String  -- ^ block
       -> Maybe String  -- ^ op
       -> Handler [ OpOriginationsDetail ]
originationsV1 p mpg mnu ma mb mo = liftIO $ lastesOriginations p pg nu ma mb mo
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

originationsNumV1 :: Pool G.Postgresql
         -> Maybe String  -- ^ account
         -> Maybe String  -- ^ block
         -> Maybe String  -- ^ op
         -> Handler Int
originationsNumV1 p a b o = liftIO $ originationsNum p a b o

ballotsV1 :: Pool G.Postgresql
       -> Maybe Int     -- ^ page
       -> Maybe Int     -- ^ number
       -> Maybe String  -- ^ account
       -> Maybe String  -- ^ block
       -> Maybe String  -- ^ op
       -> Maybe String  -- ^ proposals
       -> Handler [ OpBallotsDetail ]
ballotsV1 p mpg mnu ma mb mo mp = liftIO $ lastesBallots p pg nu ma mb mo mp
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

ballotsNumV1 :: Pool G.Postgresql
         -> Maybe String  -- ^ account
         -> Maybe String  -- ^ block
         -> Maybe String  -- ^ op
         -> Maybe String  -- ^ proposals
         -> Handler Int
ballotsNumV1 p a b o s = liftIO $ ballotsNum p a b o s

proposalsV1 :: Pool G.Postgresql
       -> Maybe Int     -- ^ page
       -> Maybe Int     -- ^ number
       -> Maybe String  -- ^ account
       -> Maybe String  -- ^ block
       -> Maybe String  -- ^ op
       -> Handler [ OpProposalsDetail ]
proposalsV1 p mpg mnu ma mb mo = liftIO $ lastesProposals p pg nu ma mb mo
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

proposalsNumV1 :: Pool G.Postgresql
         -> Maybe String  -- ^ account
         -> Maybe String  -- ^ block
         -> Maybe String  -- ^ op
         -> Handler Int
proposalsNumV1 p a b o = liftIO $ proposalsNum p a b o

proposersV1 :: Pool G.Postgresql
       -> Maybe Int     -- ^ page
       -> Maybe Int     -- ^ number
       -> Handler [ Proposers ]
proposersV1 p mpg mnu = liftIO $ lastesProposers p pg nu
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

proposersNumV1 :: Pool G.Postgresql
         -> Handler Int
proposersNumV1 o = liftIO $ proposersNum o

activateAccountsV1 :: Pool G.Postgresql
       -> Maybe Int     -- ^ page
       -> Maybe Int     -- ^ number
       -> Maybe String  -- ^ account
       -> Maybe String  -- ^ block
       -> Maybe String  -- ^ op
       -> Handler [ OpActivateAccountsDetail ]
activateAccountsV1 p mpg mnu ma mb mo = liftIO $ lastesActivateAccounts p pg nu ma mb mo
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

activateAccountsNumV1 :: Pool G.Postgresql
         -> Maybe String  -- ^ account
         -> Maybe String  -- ^ block
         -> Maybe String  -- ^ op
         -> Handler Int
activateAccountsNumV1 p a b o = liftIO $ activateAccountsNum p a b o

revealsV1 :: Pool G.Postgresql
       -> Maybe Int     -- ^ page
       -> Maybe Int     -- ^ number
       -> Maybe String  -- ^ account
       -> Maybe String  -- ^ block
       -> Maybe String  -- ^ op
       -> Handler [ OpRevealsDetail ]
revealsV1 p mpg mnu ma mb mo = liftIO $ lastesReveals p pg nu ma mb mo
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

revealsNumV1 :: Pool G.Postgresql
         -> Maybe String  -- ^ account
         -> Maybe String  -- ^ block
         -> Maybe String  -- ^ op
         -> Handler Int
revealsNumV1 p a b o = liftIO $ revealsNum p a b o

seedNonceEvelationsV1 :: Pool G.Postgresql
       -> Maybe Int     -- ^ page
       -> Maybe Int     -- ^ number
       -> Maybe String  -- ^ block
       -> Maybe String  -- ^ op
       -> Handler [ OpSeedNonceRevelationsDetail ]
seedNonceEvelationsV1 p mpg mnu mb mo = liftIO $ lastesSeedNonceEvelations p pg nu mb mo
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

seedNonceEvelationsNumV1 :: Pool G.Postgresql
         -> Maybe String  -- ^ block
         -> Maybe String  -- ^ op
         -> Handler Int
seedNonceEvelationsNumV1 p b o = liftIO $ seedNonceEvelationsNum p b o

doubleBakingEvidencesV1 :: Pool G.Postgresql
       -> Maybe Int     -- ^ page
       -> Maybe Int     -- ^ number
       -> Maybe String  -- ^ block
       -> Maybe String  -- ^ op
       -> Handler [ OpDoubleBakingEvidencesDetail ]
doubleBakingEvidencesV1 p mpg mnu mb mo = liftIO $ lastesDoubleBakingEvidences p pg nu mb mo
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

doubleBakingEvidencesNumV1 :: Pool G.Postgresql
         -> Maybe String  -- ^ block
         -> Maybe String  -- ^ op
         -> Handler Int
doubleBakingEvidencesNumV1 p b o = liftIO $ doubleBakingEvidencesNum p b o

doubleEndorsementEvidencesV1 :: Pool G.Postgresql
       -> Maybe Int     -- ^ page
       -> Maybe Int     -- ^ number
       -> Maybe String  -- ^ block
       -> Maybe String  -- ^ op
       -> Handler [ OpDoubleEndorsementEvidencesDetail ]
doubleEndorsementEvidencesV1 p mpg mnu mb mo = liftIO $ lastesDoubleEndorsementEvidences p pg nu mb mo
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

doubleEndorsementEvidencesNumV1 :: Pool G.Postgresql
         -> Maybe String  -- ^ block
         -> Maybe String  -- ^ op
         -> Handler Int
doubleEndorsementEvidencesNumV1 p b o = liftIO $ doubleEndorsementEvidencesNum p b o

protocolsV1 :: Pool G.Postgresql
       -> Maybe Int     -- ^ page
       -> Maybe Int     -- ^ number
       -> Handler [ Protocols ]
protocolsV1 p mpg mnu = liftIO $ lastesProtocols p pg nu
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

protocolsNumV1 :: Pool G.Postgresql
         -> Handler Int
protocolsNumV1 o = liftIO $ protocolsNum o

twentyFourStatV1 :: Pool G.Postgresql
               -> Handler Stat
twentyFourStatV1 o = liftIO $ twentyFourStat o

accountsV1 :: Pool G.Postgresql
           -> Maybe Int     -- ^ page
           -> Maybe Int     -- ^ number
           -> Maybe String
           -> Handler [Accounts]
accountsV1 p mpg mnu mpr =  liftIO $ accountLists p pg nu mpr
    where pg = fromMaybe defaultPage mpg
          nu = fromMaybe defaultNumberPerPage mnu

accountsNumV1 :: Pool G.Postgresql
              -> Maybe String
              -> Handler Int
accountsNumV1 o mpr = liftIO $ accountsNum o mpr

