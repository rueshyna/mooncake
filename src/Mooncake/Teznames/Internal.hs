{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
--
module Mooncake.Teznames.Internal where
--
import GHC.Generics
--
import Control.Monad (liftM)
--
import Data.Aeson
import Data.Aeson.Types
import Data.Text as T
import Data.Vector as V hiding ((++))
import Data.Maybe as M
import Data.HashMap.Lazy as HM
--
import Data.Time as Time
import Data.Time.RFC3339
--
type BaseText = String
type RawName = BaseText
type PackedName = BaseText
type HashedName = BaseText
--
getStorage :: FromJSON a => Object -> Parser a
getStorage obj = (obj .: "script") >>= (.: "storage")
--
arg0 :: FromJSON a => Object -> Parser a
arg0 obj = (obj .: "args") >>= (return . (V.! 0))
arg1 :: FromJSON a => Object -> Parser a
arg1 obj = (obj .: "args") >>= (return . (V.! 1))
--
getNField :: FromJSON a => Int -> Object -> Parser a
getNField 0 obj = arg0 obj
getNField n obj = arg1 obj >>= getNField (n - 1)
--
data PackedData = PackedData
  { packed :: String
  , gas :: String
  } deriving (Show, Generic)
instance ToJSON PackedData
instance FromJSON PackedData
--
data RootnameAddress = RootnameAddress
  { rootnameAddr :: String
  } deriving (Show, Generic)
instance ToJSON RootnameAddress
instance FromJSON RootnameAddress where
  parseJSON = withObject "rootnameAddr" $ (\o ->
    getStorage o >>= (getNField 2) >>= (.: "string") >>= (return . RootnameAddress))
--
data Subnames = Subnames
  { records :: HashMap String String
  } deriving (Show, Eq, Generic)
instance ToJSON Subnames
instance FromJSON Subnames where
  parseJSON = withObject "Subnames" $ (\o ->
    getStorage o >>= (getNField 5) >>= (return . Subnames . parseArray2HM))
--
parseArray2HM :: Value -> HashMap String String
parseArray2HM (Array arr) =
  HM.fromList $ M.catMaybes
    $ Prelude.map parseArray2MaybePair
    $ V.toList arr
parseArray2HM _ = HM.empty
--
parseArray2MaybePair :: Value -> Maybe (String, String)
parseArray2MaybePair (Object hm) = case HM.lookup "args" hm of
  Just (Array arr) -> do
    (v1, v2) <- just2Ele arr
    vv2MaybePair v1 v2
  _ -> Nothing
parseArray2MaybePair _ = Nothing
--
just2Ele :: Array -> Maybe (Value, Value)
just2Ele arr = case (V.toList arr) of
  [v1,v2] -> return (v1,v2)
  _ -> Nothing
--
vv2MaybePair :: Value -> Value -> Maybe (String, String)
vv2MaybePair (Object mh1) (Object mh2) =
  case (HM.toList mh1, HM.toList mh2) of
    ([("bytes",v1)] ,[("string",v2)]) -> do
      va <- deString v1
      vb <- deString v2
      return (va,vb)
    ([("string",v1)] ,[("bytes",v2)]) -> do
      va <- deString v2
      vb <- deString v1
      return (va,vb)
    _ -> Nothing
vv2MaybePair _ _ = Nothing
--
deString :: Value -> Maybe String
deString (String s) = Just $ T.unpack s
deString _ = Nothing
--
data RRecord = RRecord
  { name :: String
  , address :: String
  , amount :: String
  , timestamp :: String
  } deriving (Show, Generic)
instance ToJSON RRecord
instance FromJSON RRecord where
  parseJSON = withObject "RRecord" $ \o -> do
    xs <- o .: "args"
    let x0 = getArray xs 0
    let x1 = getArray xs 1
    n <- x0 .: "string"
    vs <- x1 .: "args"
    let v0 = getArray vs 0
    let v1 = getArray vs 1
    a <- v0 .: "string"
    us <- v1 .: "args"
    let u0 = getArray us 0
    let u1 = getArray us 1
    b <- u0 .: "int"
    c <- u1 .: "string"
    return $ RRecord n a b c
--
getArray :: Value -> Int -> Object
getArray (Array v) n = case (V.! n) v of
  (Object o) -> o
  _ -> error $ "getArray["++(show v)++"]"
getArray o _ = error $ "getArray["++(show o)++"]"
--
rrTransfer :: [RRecord] -> HashMap String (String,String,String)
rrTransfer [] = HM.empty
rrTransfer ((RRecord n a b c):rs) = HM.insert n (a,b,c) (rrTransfer rs)
--
nArgs1 :: (FromJSON b) => Int -> Object -> Parser b
nArgs1 0 obj = (obj .: "args") >>= (return . (V.! 1))
nArgs1 n obj = (obj .: "args") >>= (return . (V.! 1)) >>= nArgs1 (n-1)
--
data Registrations = Registrations
  { regs :: HashMap String (String,String,String) }
  deriving (Show, Generic)
instance ToJSON Registrations
instance FromJSON Registrations where
  parseJSON = withObject "regs" $ \o -> do
    x1 <- o  .: "script"
    x2 <- x1 .: "storage"
    x3 <- nArgs1 3 x2
    case x3 of
      (Array arr) -> liftM (Registrations . rrTransfer) $
                      Prelude.mapM (parseJSON :: Value -> Parser RRecord) (V.toList arr)
      _ -> return $ Registrations $ rrTransfer []
--
parseTimeToUTC :: String -> Maybe UTCTime
parseTimeToUTC = (liftM zonedTimeToUTC) . parseTimeRFC3339
--
data TriDates = TriDates
  { registrationDate :: Time.UTCTime
  , expirationDate   :: Time.UTCTime
  , modificationDate :: Time.UTCTime
  , modificationText :: String
  -- { somev :: String
  } deriving (Show, Generic)
instance ToJSON TriDates where
  toJSON (TriDates t1 t2 t3 msg) = object $
    [ ("registrationDate", String $ T.pack $ showTime t1 )
    , ("expirationDate"  , String $ T.pack $ showTime t2 )
    , ("modificationDate", String $ T.pack $ showTime t3 )
    , ("modificationText", String $ T.pack msg )
    ]
instance FromJSON TriDates where
  parseJSON = withObject "TriDates" $ \o -> do
    xs <- o .: "args"
    let x0 = getArray xs 0
    let x1 = getArray xs 1
    t1 <- x0 .: "string"
    vs <- x1 .: "args"
    let v0 = getArray vs 0
    let v1 = getArray vs 1
    t2 <- v0 .: "string"
    us <- v1 .: "args"
    let u0 = getArray us 0
    let u1 = getArray us 1
    t3 <- u0 .: "string"
    msg <- u1 .: "string"
    case (parseTimeToUTC t1, parseTimeToUTC t2, parseTimeToUTC t3) of
      (Just t1', Just t2', Just t3') -> return (TriDates t1' t2' t3' msg)
      _ -> do
        let ct = M.fromJust $ parseTimeToUTC "2000-01-01T00:00:00+08:00"
        return (TriDates ct ct ct "[ERROR] cannot parse as RFC3339")
--
showTime :: Time.UTCTime -> String
showTime t = formatTime defaultTimeLocale "%FT%TZ" t
--
data TriDateInfo = TriDateInfo
  { somev :: TriDates
  } deriving (Show, Generic)
instance ToJSON TriDateInfo where
  toJSON (TriDateInfo td) = toJSON td
instance FromJSON TriDateInfo where
  parseJSON = withObject "3dates" $ \o -> do
    o2 <- o  .: "script"
    o3 <- o2 .: "storage"
    x3 <- nArgs1 5 o3
    out <- parseJSON x3
    return $ TriDateInfo out
--
