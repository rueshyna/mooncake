{-# LANGUAGE OverloadedStrings #-}
--
module Mooncake.Teznames.V04.App
  ( getTeznameAddr
  , getDest
  , getOwner
  , getGateAddr
  , hashWord
  , getSubnames
  , getRegistrationList
  , timestamps
  , getRegDate
  , getExpDate
  , getModDate
  , isExpiredApp
  , isRegisterableApp
  ) where
--
import Mooncake.Common.Config as Config
--
import Mooncake.Teznames.Utils
import Mooncake.Teznames.RPC
import Mooncake.Teznames.JSON
import Mooncake.Teznames.Internal
--
import Servant
import Control.Monad (liftM)
import Control.Monad.IO.Class  (liftIO)
import Data.HashMap.Lazy as HM
import Network.HTTP.Simple
import Data.Time as Time
--
--
getTeznameAddr
  :: String -- ^ name
  -> Handler (Maybe JSON_TeznameAddr)
getTeznameAddr name' = liftIO $ do
  mrta <- tzRPCRootnameAddress
  mts <- getTeznameReady name'
  case (mrta, mts) of
    (Just rtaddr, Just ts) ->
      liftM (liftM JSON_TeznameAddr) $
        findNextNameAddr (rootnameAddr rtaddr) ts
    _ -> return Nothing
--
findNextNameAddr :: String -> [HashedName] -> IO (Maybe String)
findNextNameAddr addr [] = return (Just addr)
findNextNameAddr addr (t1:ts) = do
  mtargetAddr <- addrName2NextAddr addr t1
  case mtargetAddr of
    (Just targetAddr) -> findNextNameAddr targetAddr ts
    Nothing -> return Nothing
--
addrName2NextAddr :: String -> String -> IO (Maybe String)
addrName2NextAddr addr target = do
  mhm <- tzRPCSubnames addr
  case mhm of
    (Just hm) -> return $ HM.lookup target hm
    Nothing -> return Nothing
--
getDest
  :: String -- ^ address
  -> Handler (Maybe JSON_Dest)
getDest addr = liftIO $ do
  request <- tzRPCRequest Mooncake.Teznames.RPC.GET $
              [ "/chains/main/blocks/head/context/contracts/", addr ]
  response <- httpJSON request
  return $ is200 response
--
getOwner
  :: String -- ^ address
  -> Handler (Maybe JSON_Owner)
getOwner addr = liftIO $ do
  request <- tzRPCRequest Mooncake.Teznames.RPC.GET $
              [ "/chains/main/blocks/head/context/contracts/", addr ]
  response <- httpJSON request
  return $ is200 response
--
getGateAddr
  :: Handler JSON_GateAddr
getGateAddr = liftIO (return $ JSON_GateAddr Config.gateAddress)
--
hashWord
  :: String -- ^ word
  -> Handler (Maybe JSON_HashWord)
hashWord w = liftIO $ do
  mpd <- tzRPCPack w
  return $ liftM (JSON_HashWord . hasher . packed) mpd
--
getSubnames
  :: String -- ^ address
  -> Handler (Maybe (HashMap String String))
getSubnames addr = liftIO $ tzRPCSubnames addr
--
getRegistrationList
  :: Handler (Maybe JSON_RegistrationList)
getRegistrationList = liftIO $ do
  request <- tzRPCRequest Mooncake.Teznames.RPC.GET $
              [ "/chains/main/blocks/head/context/contracts/"
              , Config.gateAddress ]
  response <- (httpJSON request) :: IO (Response Registrations)
  return $ liftM (JSON_RegistrationList . regs) $ is200 response
--
timestamps
  :: String -- ^ address
  -> Handler (Maybe JSON_TriDateInfo)
timestamps addr = liftIO $ do
  request <- tzRPCRequest Mooncake.Teznames.RPC.GET $
              [ "/chains/main/blocks/head/context/contracts/", addr ]
  response <- httpJSON request :: IO (Response JSON_TriDateInfo)
  return $ is200 response
--
getRegDate
  :: String -- ^ address
  -> Handler (Maybe JSON_RegTimestamp)
getRegDate addr = liftIO $ do
  request <- tzRPCRequest Mooncake.Teznames.RPC.GET $
              [ "/chains/main/blocks/head/context/contracts/", addr ]
  response <- httpJSON request :: IO (Response JSON_TriDateInfo)
  return $ (fmap (JSON_RegTimestamp . registrationDate . somev)) $ is200 response
getExpDate
  :: String -- ^ address
  -> Handler (Maybe JSON_ExpTimestamp)
getExpDate addr = liftIO $ do
  request <- tzRPCRequest Mooncake.Teznames.RPC.GET $
              [ "/chains/main/blocks/head/context/contracts/", addr ]
  response <- httpJSON request :: IO (Response JSON_TriDateInfo)
  return $ (fmap (JSON_ExpTimestamp . expirationDate . somev)) $ is200 response
--
getModDate
  :: String -- ^ address
  -> Handler (Maybe JSON_ModTimestamp)
getModDate addr = liftIO $ do
  request <- tzRPCRequest Mooncake.Teznames.RPC.GET $
              [ "/chains/main/blocks/head/context/contracts/", addr ]
  response <- httpJSON request :: IO (Response JSON_TriDateInfo)
  return $ (fmap (JSON_ModTimestamp . modificationDate . somev)) $ is200 response
--
isExpiredApp
  :: String -- ^ address
  -> Handler (Maybe JSON_IsExpired)
isExpiredApp tezname = do
  mAddr <- getTeznameAddr tezname
  case mAddr of
    (Just jaddr) -> do
        mExpTime <- getExpDate (teznameAddr jaddr)
        case mExpTime of
          Just (JSON_ExpTimestamp expTime) -> liftIO $ do
              now <- Time.getCurrentTime
              if ( Time.diffUTCTime now expTime > 0 )
                then return $ Just $ JSON_IsExpired
                      { isExisting = True, isExpired = True }
                else return $ Just $ JSON_IsExpired
                      { isExisting = True, isExpired = False }
          Nothing -> throwError err422 { errBody = "Unresolvable expiration date" }
    Nothing -> return $ Just $ JSON_IsExpired { isExisting = False, isExpired = False }
--
isRegisterableApp
  :: String -- ^ name
  -> Handler (Maybe JSON_IsRegisterable)
isRegisterableApp tezname = do
  mAddr <- getTeznameAddr tezname
  case mAddr of
    Just jaddr -> do
      mExpTime <- getExpDate (teznameAddr jaddr)
      case mExpTime of
        Just (JSON_ExpTimestamp expTime) -> liftIO $ do
          now <- Time.getCurrentTime
          let msec = 30 * 24 * 60 * 60
              dsec = Time.diffUTCTime expTime now
          if ( dsec <= msec )
            then return $ Just $ JSON_IsRegisterable True (floor dsec)
            else return $ Just $ JSON_IsRegisterable False (floor dsec)
        Nothing -> throwError err422 { errBody = "Unresolvable expiration date" }
    Nothing -> return $ Just $ JSON_IsRegisterable True 0
--
