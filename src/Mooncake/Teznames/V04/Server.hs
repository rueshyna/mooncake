{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE LambdaCase #-}
--
module Mooncake.Teznames.V04.Server
  ( APITeznamesV04
  , serverTeznamesV04
  ) where
--
import Mooncake.Teznames.Utils
import Mooncake.Teznames.JSON
import Mooncake.Teznames.V04.App
--
import Servant
import Data.Char (isAlphaNum)
import Data.Maybe (isJust)
import Control.Monad.IO.Class  (liftIO)
--
type APITeznamesV04 =
  "teznames" :> "v0.4" :> "babylon"
             :> "getGateAddr"
             :> Get '[JSON] JSON_GateAddr
  :<|> "teznames" :> "v0.4" :> "babylon"
                  :> "hashWord"
                  :> QueryParam "word" String
                  :> Get '[JSON] JSON_HashWord
  -- status
  :<|> "teznames" :> "v0.4" :> "babylon"
                  :> "status" :> "isRegistered"
                  :> QueryParam "name" String
                  :> Get '[JSON] JSON_IsRegistered
  :<|> "teznames" :> "v0.4" :> "babylon"
                  :> "status" :> "isExpired"
                  :> QueryParam "name" String
                  :> Get '[JSON] JSON_IsExpired
  :<|> "teznames" :> "v0.4" :> "babylon"
                  :> "status" :> "isRegisterable"
                  :> QueryParam "name" String
                  :> Get '[JSON] JSON_IsRegisterable
  -- info
  :<|> "teznames" :> "v0.4" :> "babylon"
                  :> "info" :> "getTeznameAddr"
                  :> QueryParam "name" String
                  :> Get '[JSON] JSON_TeznameAddr
  :<|> "teznames" :> "v0.4" :> "babylon"
                  :> "info" :> "getDestFromName"
                  :> QueryParam "name" String
                  :> Get '[JSON] JSON_Dest
  :<|> "teznames" :> "v0.4" :> "babylon"
                  :> "info" :> "getDest"
                  :> QueryParam "address" String
                  :> Get '[JSON] JSON_Dest
  :<|> "teznames" :> "v0.4" :> "babylon"
                  :> "info" :> "getOwner"
                  :> QueryParam "address" String
                  :> Get '[JSON] JSON_Owner
  :<|> "teznames" :> "v0.4" :> "babylon"
                  :> "info" :> "getSubnames"
                  :> QueryParam "address" String
                  :> Get '[JSON] JSON_Subnames
  :<|> "teznames" :> "v0.4" :> "babylon"
                  :> "info" :> "registrationList"
                  :> Get '[JSON] JSON_RegistrationList
  -- info/timestamps
  :<|> "teznames" :> "v0.4" :> "babylon"
                  :> "info" :> "timestamps"
                  :> QueryParam "address" String
                  :> Get '[JSON] JSON_TriDateInfo
  :<|> "teznames" :> "v0.4" :> "babylon"
                  :> "info" :> "timestamps" :> "getRegDate"
                  :> QueryParam "address" String
                  :> Get '[JSON] JSON_RegTimestamp
  :<|> "teznames" :> "v0.4" :> "babylon"
                  :> "info" :> "timestamps" :> "getExpDate"
                  :> QueryParam "address" String
                  :> Get '[JSON] JSON_ExpTimestamp
  :<|> "teznames" :> "v0.4" :> "babylon"
                  :> "info" :> "timestamps" :> "getModDate"
                  :> QueryParam "address" String
                  :> Get '[JSON] JSON_ModTimestamp
--
serverTeznamesV04 :: Server APITeznamesV04
serverTeznamesV04 =
  getGateAddrV04
  :<|> hashWordV04
  :<|> isRegisteredV04
  :<|> isExpiredV04
  :<|> isRegisterableV04
  :<|> getTeznameAddrV04
  :<|> getDestFromNameV04
  :<|> getDestV04
  :<|> getOwnerV04
  :<|> getSubnamesV04
  :<|> getRegistrationListV04
  :<|> timestampsV04
  :<|> getRegDateV04
  :<|> getExpDateV04
  :<|> getModDateV04
--
getGateAddrV04 :: Handler JSON_GateAddr
getGateAddrV04 = getGateAddr
--
hashWordV04 :: Maybe String -> Handler JSON_HashWord
hashWordV04 (Just wstr)
  | (and $ Prelude.map isAlphaNum wstr) =
      hashWord wstr >>= (\mhw -> case mhw of
        { Just o -> liftIO $ return o
        ; Nothing -> throwError err422 { errBody = "Process failed" } })
  | otherwise = throwError err422 { errBody = "Invalid format" }
hashWordV04 Nothing = throwError err400
--
isRegisteredV04 :: Maybe String -> Handler JSON_IsRegistered
isRegisteredV04 (Just tezname) =
  (getTeznameAddr tezname) >>= (liftIO . return . JSON_IsRegistered . isJust)
isRegisteredV04 Nothing = throwError err400
--
isExpiredV04 :: Maybe String -> Handler JSON_IsExpired
isExpiredV04 (Just tezname) =
  isExpiredApp tezname >>= (\ms -> case ms of
    { Just jta -> liftIO $ return $ jta
    ; Nothing -> throwError err422 { errBody = "Internal unknown error" } })
isExpiredV04 Nothing = throwError err400
--
isRegisterableV04  :: Maybe String -> Handler JSON_IsRegisterable
isRegisterableV04 (Just tezname) =
  isRegisterableApp tezname >>= (\ms -> case ms of
    { Just jta -> liftIO $ return $ jta
    ; Nothing -> throwError err422 { errBody = "Internal unknown error" } })
isRegisterableV04 Nothing = throwError err400
--
getTeznameAddrV04 :: Maybe String -> Handler JSON_TeznameAddr
getTeznameAddrV04 (Just tezname) =
  getTeznameAddr tezname >>= (\ms -> case ms of
    { Just jta -> liftIO $ return $ jta
    ; Nothing -> throwError err422 { errBody = "The input name is unregistered" } })
getTeznameAddrV04 Nothing = throwError err400
--
getDestV04 :: Maybe String -> Handler JSON_Dest
getDestV04 (Just addr)
  | isWalletAddr addr = getDest addr >>= (\mhm -> case mhm of
      { Just d -> liftIO $ return d
      ; Nothing -> throwError err422 { errBody = "Process failed" } })
  | otherwise = throwError err422 { errBody = "Not a valid address" }
getDestV04 Nothing = throwError err400
--
getOwnerV04 :: Maybe String -> Handler JSON_Owner
getOwnerV04 (Just addr)
  | isWalletAddr addr = getOwner addr >>= (\mhm -> case mhm of
      { Just o -> liftIO $ return o
      ; Nothing -> throwError err422 { errBody = "Process failed" } })
  | otherwise = throwError err422 { errBody = "Not a valid address" }
getOwnerV04 Nothing = throwError err400
--
getSubnamesV04 :: Maybe String -> Handler JSON_Subnames
getSubnamesV04 (Just addr)
  | isWalletAddr addr = getSubnames addr >>= (\mhm -> case mhm of
      { Just hm -> liftIO $ return $ JSON_Subnames hm
      ; Nothing -> throwError err422 { errBody = "Process failed" } })
  | otherwise = throwError err422 { errBody = "Not a valid address" }
getSubnamesV04 Nothing = throwError err400
--
getRegistrationListV04 :: Handler JSON_RegistrationList
getRegistrationListV04 = getRegistrationList >>= (\mrs -> case mrs of
  { Just rs -> liftIO $ return rs
  ; Nothing -> throwError err422 { errBody = "ERROR from getRegistrationList" } })
--
timestampsV04 :: Maybe String -> Handler JSON_TriDateInfo
timestampsV04 (Just addr)
  | isWalletAddr addr = timestamps addr >>= (\mtdi -> case mtdi of
      { Just o -> liftIO $ return o
      ; Nothing -> throwError err422 { errBody = "Process failed" } })
  | otherwise = throwError err422 { errBody = "Not a valid address" }
timestampsV04 Nothing = throwError err400
--
getRegDateV04 :: Maybe String -> Handler JSON_RegTimestamp
getRegDateV04 (Just addr)
  | isWalletAddr addr = getRegDate addr >>= (\mtdi -> case mtdi of
      { Just o -> liftIO $ return o
      ; Nothing -> throwError err422 { errBody = "Process failed" } })
  | otherwise = throwError err422 { errBody = "Not a valid address" }
getRegDateV04 Nothing = throwError err400

getExpDateV04 :: Maybe String -> Handler JSON_ExpTimestamp
getExpDateV04 (Just addr)
  | isWalletAddr addr = getExpDate addr >>= (\mtdi -> case mtdi of
      { Just o -> liftIO $ return o
      ; Nothing -> throwError err422 { errBody = "Process failed" } })
  | otherwise = throwError err422 { errBody = "Not a valid address" }
getExpDateV04 Nothing = throwError err400

getModDateV04 :: Maybe String -> Handler JSON_ModTimestamp
getModDateV04 (Just addr)
  | isWalletAddr addr = getModDate addr >>= (\mtdi -> case mtdi of
      { Just o -> liftIO $ return o
      ; Nothing -> throwError err422 { errBody = "Process failed" } })
  | otherwise = throwError err422 { errBody = "Not a valid address" }
getModDateV04 Nothing = throwError err400

getDestFromNameV04 :: Maybe String -> Handler JSON_Dest
getDestFromNameV04 (Just tezname) =
  getTeznameAddr tezname >>= (\ms -> case ms of
    { Just jta -> getDestV04 $ Just $ teznameAddr jta
    ; Nothing -> throwError err422 { errBody = "The input name is unregistered" } })
getDestFromNameV04 Nothing = throwError err400
