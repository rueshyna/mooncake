{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
--
module Mooncake.Teznames.RPC where
--
import Mooncake.Common.Config as Config
--
import Mooncake.Teznames.Internal
--
import Data.Text as T
import Data.Aeson
import Network.HTTP.Simple
import Data.HashMap.Lazy as HM
--
data Method = GET | POST deriving (Show, Eq)
--
tzRPCRequest :: Method -> [String] -> IO Request
tzRPCRequest m args =
  parseRequest $ ( (show m) ++ " " ++ Config.tezosNodeURL ++ (Prelude.concat args))
--
tzRPCRootnameAddress :: IO (Maybe RootnameAddress)
tzRPCRootnameAddress = do
  request <- tzRPCRequest GET $ [ "/chains/main/blocks/head/context/contracts/"
                                , Config.gateAddress ]
  response <- (httpJSON request) :: IO (Response RootnameAddress)
  return $ is200 response
--
tzRPCPack :: RawName -> IO (Maybe PackedData)
tzRPCPack str = do
  let requestJSON = object
            [ ("data", object [("string", String $ T.pack str)])
            , ("type", object [("prim", String "string")])
            , ("gas", String "800000")
            ]
  request <- tzRPCRequest POST
            $ [ "/chains/main/blocks/head/"
              , "helpers/scripts/pack_data" ]
  response <- httpJSON
            $ setRequestHeader "Content-Type" ["application/json"]
            $ setRequestBodyJSON requestJSON
            $ request
  return $ is200 response
--
tzRPCSubnames :: String -> IO (Maybe (HashMap String String))
tzRPCSubnames addr = do
  request <- tzRPCRequest GET $ [ "/chains/main/blocks/head/context/contracts/", addr ]
  response <- (httpJSON request) :: IO (Response Subnames)
  return (records <$> is200 response)
--
is200 :: Response a -> Maybe a
is200 res = case getResponseStatusCode res of
  200 -> Just $ getResponseBody res
  _   -> Nothing
--
