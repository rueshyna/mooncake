{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
--
module Mooncake.Teznames.JSON where
--
import GHC.Generics
--
import Mooncake.Teznames.Internal
--
import Data.Aeson
import Data.Text as T
import Data.Vector as V (fromList)
import Data.HashMap.Lazy as HM
import Data.Time as Time
--
data JSON_Dest = JSON_Dest
  { dest :: String
  } deriving (Show, Generic)
instance ToJSON JSON_Dest
instance FromJSON JSON_Dest where
  parseJSON = withObject "dest" $ (\o ->
    getStorage o >>= (getNField 2) >>= (.: "string") >>= (return . JSON_Dest))
--
data JSON_Owner = JSON_Owner
  { owner :: String
  } deriving (Show, Generic)
instance ToJSON JSON_Owner
instance FromJSON JSON_Owner where
  parseJSON = withObject "owner" $ (\o ->
    getStorage o >>= (getNField 3) >>= (.: "string") >>= (return . JSON_Owner))
--
data JSON_GateAddr = JSON_GateAddr
  { gateAddr :: String
  } deriving (Show, Generic)
instance ToJSON JSON_GateAddr
instance FromJSON JSON_GateAddr
--
data JSON_HashWord = JSON_HashWord
  { hword :: String
  } deriving (Show, Generic)
instance ToJSON JSON_HashWord
instance FromJSON JSON_HashWord
--
data JSON_IsRegistered = JSON_IsRegistered
  { isRegistered :: Bool
  } deriving (Show, Generic)
instance ToJSON JSON_IsRegistered
instance FromJSON JSON_IsRegistered
--
data JSON_IsExpired = JSON_IsExpired
  { isExisting :: Bool
  , isExpired :: Bool
  } deriving (Show, Generic)
instance ToJSON JSON_IsExpired
instance FromJSON JSON_IsExpired
--
data JSON_IsRegisterable = JSON_IsRegisterable
  { isRegisterable :: Bool
  , diffTime :: Integer
  } deriving (Show, Generic)
instance ToJSON JSON_IsRegisterable
instance FromJSON JSON_IsRegisterable
--
data JSON_TeznameAddr = JSON_TeznameAddr
  { teznameAddr :: String
  } deriving (Show, Generic)
instance ToJSON JSON_TeznameAddr
instance FromJSON JSON_TeznameAddr
--
data JSON_Subnames
  = JSON_Subnames (HM.HashMap String String)
  deriving (Show, Generic)
instance FromJSON JSON_Subnames
instance ToJSON JSON_Subnames where
  toJSON (JSON_Subnames hmap)
    = Array
    $ V.fromList
    $ fmap transferStep
    $ HM.toList hmap
    where transferStep (k,v) =
            object [ ("name"   , String $ T.pack k )
                   , ("address", String $ T.pack v ) ]
--
data JSON_RegistrationList
  = JSON_RegistrationList (HM.HashMap String (String,String,String))
  deriving (Show, Generic)
instance FromJSON JSON_RegistrationList
instance ToJSON JSON_RegistrationList where
  toJSON (JSON_RegistrationList hmap)
    = Array
    $ V.fromList
    $ fmap transferStep
    $ HM.toList hmap
    where transferStep (k, (ad, am, ti)) = object
              [ ("name", String $ T.pack k)
              , ("address", String $ T.pack ad)
              , ("amount", String $ T.pack am)
              , ("timestamp", String $ T.pack ti)
              ]
--
type JSON_TriDateInfo = TriDateInfo
--
data JSON_RegTimestamp =
  JSON_RegTimestamp Time.UTCTime
  deriving (Show, Generic)
instance FromJSON JSON_RegTimestamp
instance ToJSON JSON_RegTimestamp where
  toJSON (JSON_RegTimestamp t) = object $
    [("registrationDate", String $ T.pack $ showTime t )]
--
data JSON_ExpTimestamp =
  JSON_ExpTimestamp Time.UTCTime
  deriving (Show, Generic)
instance FromJSON JSON_ExpTimestamp
instance ToJSON JSON_ExpTimestamp where
  toJSON (JSON_ExpTimestamp t) = object $
    [("expirationDate", String $ T.pack $ showTime t )]
--
data JSON_ModTimestamp =
  JSON_ModTimestamp Time.UTCTime
  deriving (Show, Generic)
instance FromJSON JSON_ModTimestamp
instance ToJSON JSON_ModTimestamp where
  toJSON (JSON_ModTimestamp t) = object $
    [("modificationDate", String $ T.pack $ showTime t )]
