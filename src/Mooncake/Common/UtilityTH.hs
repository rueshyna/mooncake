module Mooncake.Common.UtilityTH where

import Control.Monad

import Language.Haskell.TH

import Database.Groundhog.TH

-- uncurry1 :: (a -> b -> c) -> (a, b) -> c
-- uncurry2 :: \ f (x1, x2) -> f x1 x2
-- uncurry3 :: \ f (x1, x2, x3) -> f x1 x2 x3
-- uncurry4 :: \ f (x1, x2, x3, x4) -> f x1 x2 x3 x4
-- ...
uncurryN :: Int -> Q Exp
uncurryN n = do
  f  <- newName "f"
  xs <- replicateM n (newName "x")
  let ntup = VarP f : [TupP (map VarP xs)]
      args = VarE f : map VarE xs
  return $ LamE ntup (foldl1 AppE args)

-- curry1  = \ f x1 -> f (x1)
-- curry2  = \ f x1 x2 -> f (x1, x2)
-- curry3  = \ f x1 x2 x3 -> f (x1, x2, x3)
-- curry4  = \ f x1 x2 x3 x4 -> f (x1, x2, x3, x4)
-- ...
curryN :: Int -> Q Exp
curryN n = do
  f  <- newName "f"
  xs <- replicateM n (newName "x")
  let args = map VarP (f:xs)
      ntup = TupE (map VarE xs)
  return $ LamE args (AppE (VarE f) ntup)


-- tupToList2 = \ (x1, x2) -> [x1, x2]
-- tupToList3 = \ (x1, x2, x3) -> [x1, x2, x3]
tupToListN :: Int -> Q Exp
tupToListN n = do
  xs <- replicateM n (newName "x")
  let xstup = TupP (map VarP xs)
      xslist = ListE (map VarE xs)
  return $ LamE [xstup] xslist

-- zipT2 = \ (x1, x2) (y1, y2) -> ((x1, y1), (x2, y2))
-- zipT3 = \ (x1, x2, x3) (y1, y2, y3) -> ((x1, y1), (x2, y2), (x3, y3))
zipTN :: Int -> Q Exp
zipTN n = do
  xs <- replicateM n (newName "x")
  ys <- replicateM n (newName "y")
  zs <- replicateM n $ sequence [newName "x", newName "y"]
  let xstup = TupP (map VarP xs)
      ystup = TupP (map VarP ys)
      z = TupE $ fmap (TupE . map VarE) zs

  return $ LamE [xstup, ystup] z

-- loopTN 1 = \ f ((f1, y1)) -> (f (f1, y1))
-- loopTN 2 = \ f ((f1, y1), (f2, y2)) -> (f (f1, y1), f (f2, y2))
loopTN :: Int -> Q Exp
loopTN n = do
  f  <- newName "f"
  xs <- replicateM n $ sequence [newName "f", newName "x"]
  let xstup = VarP f : [TupP $ fmap (TupP . map VarP) xs]
      args = TupE $ fmap (AppE (VarE f) . TupE . map VarE) xs
  return $ LamE xstup args

bakingSodaNamingStyle :: NamingStyle
bakingSodaNamingStyle = persistentNamingStyle
  { mkDbFieldName = \_ _ _ fName _ -> toUnderscore fName
  }

codegenConfig :: CodegenConfig
codegenConfig = defaultCodegenConfig { namingStyle = bakingSodaNamingStyle }
