{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE GADTs                      #-}

module Mooncake.Common.DB where

--
import Control.Monad.Reader
import Control.Monad.Trans.Control
import Database.Groundhog as G
import Database.Groundhog.Core as G
import Database.Groundhog.Generic as G
import Database.Groundhog.Postgresql as G
import Database.Groundhog.Generic.Sql as G

data SQLJoin s a b where
  LeftJoin :: (G.StringLike s, G.PersistEntity a, G.PersistEntity b) => s -> a -> SQLJoin s a b -> SQLJoin s a b
  J        :: (G.StringLike s, G.PersistEntity a, G.PersistEntity b) => b -> SQLJoin s a b

selectExpr :: (G.PersistEntity a, G.PersistEntity b, G.StringLike s)
            => SQLJoin s a b -> [ s ]
selectExpr (LeftJoin _ y z) = map (addScope $ tableName_ y) (fieldNames y) <> selectExpr z
selectExpr (J x) = map (addScope $ tableName_ x) $ fieldNames x

fieldNamesInScope :: (G.PersistEntity a, G.StringLike s)
                  => a -> s -> [ s ]
fieldNamesInScope x p = map (addScope p) (fieldNames x)

strFieldNamesInScope :: (G.PersistEntity a, G.StringLike s)
                  => a -> s -> s
strFieldNamesInScope a = commasJoin . fieldNamesInScope a

addScope :: (G.StringLike s) => s -> s -> s
addScope p f = p <> "." <> f

tableExpr :: (G.PersistEntity a, G.PersistEntity b, G.StringLike s)
          => SQLJoin s a b -> [ s ]
tableExpr (LeftJoin s x y) = " from " <> tableName_ x <> " as " <>  tableName_ x : tableExpr' s "left join" y
tableExpr (J x) = [" from " <> tableName_ x]

tableExpr' :: (G.PersistEntity a, G.PersistEntity b, G.StringLike s)
           => s -> s -> SQLJoin s a b -> [ s ]
tableExpr' on op (LeftJoin s x y) =  " " <> op <> " " <> tableName_ x <> " " <> tableName_ x <> " on " <> on <> " "
                                  : tableExpr' s "left join" y
tableExpr' on op (J x) =  [ " " <> op <> " " <> tableName_ x <> " " <> tableName_ x <> " on " <> on <> " " ]

raw_ :: (PersistBackend m, MonadIO m, MonadBaseControl IO m)
     => String
     -> [ G.PersistValue ]
     -> ([[PersistValue]] -> m r)
     -> m r
raw_ s x f = do
         q <- G.queryRaw False s x
         rawValues <- G.streamToList q

         f rawValues

join_ :: (PersistBackend m, MonadIO m, MonadBaseControl IO m, G.PersistEntity a, G.PersistEntity b)
     => SQLJoin String a b
     -> ([[PersistValue]] -> m r)
     -> m r
join_ j f = do
         let query = "select "
                   <> commasJoin (selectExpr j)
                   <> unwords (tableExpr j)

         q <- G.queryRaw False query []
         rawValues <- G.streamToList q

         f rawValues

fromEntity :: (G.PersistEntity v, PersistBackend m) => [[PersistValue]] -> m ([v], [[PersistValue]])
fromEntity = fmap unzip . traverse G.fromEntityPersistValues . map (G.PersistInt64 0:)

tableName_ :: (G.PersistEntity a, G.StringLike s) => a -> s
tableName_ r = G.fromString
             $ G.entityName
             $ G.entityDef proxy r

fieldNames :: (G.PersistEntity a, G.StringLike s) => a -> [ s ]
fieldNames r = map (fromString . fst)
             $ constrParams
             $ head
             $ constructors
             $ G.entityDef proxy r

proxy :: proxy G.Postgresql
proxy = error "proxy Postgresql"

