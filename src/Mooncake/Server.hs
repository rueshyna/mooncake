{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE TypeOperators              #-}

module Mooncake.Server (runS) where

import Mooncake.TezosId.V0.Server as TV0
import Mooncake.TezosId.V1.Server as TV0
import Mooncake.Teznames.V04.Server as TN04

import Network.Wai.Handler.Warp
import Servant
import Data.String

import Control.Monad.Logger    (runStderrLoggingT)
import Database.Persist.MySQL
import System.IO
import Data.Pool

import qualified Database.Groundhog.Postgresql as G

type API
  =    APIV0
  :<|> APIV1
  :<|> APITeznamesV04
  :<|> EmptyAPI

server :: ConnectionPool -> Pool G.Postgresql -> Server API
server mp pp
  =    serverV0 mp
  :<|> serverV1 pp
  :<|> serverTeznamesV04
  :<|> emptyServer

api :: Proxy API
api = Proxy

app :: ConnectionPool -> Pool G.Postgresql -> Application
app mp pp = serve api $ server mp pp

mkApp :: ConnectionPool -> Pool G.Postgresql -> IO Application
mkApp mp pp = return $ app mp pp

runS :: String
     -> Port
     -> ConnectInfo       -- ^ MySQL connection info
     -> String            -- ^ Postgresql connection string
     -> IO ()
runS h p c s = do
    let settings =
          setPort p $
          setHost (fromString h) $
          setBeforeMainLoop (hPutStrLn stderr $ "http://" <> h <> ":" <> show p) defaultSettings

    mysqlPool <- runStderrLoggingT $ createMySQLPool c 10
    pgPool <- G.createPostgresqlPool s 10

    runSettings settings =<< mkApp mysqlPool pgPool

    destroyAllResources mysqlPool
    destroyAllResources pgPool
