{-# LANGUAGE OverloadedStrings #-}

module Mooncake.Arg
  ( Arg (..)
  , defaultDBPort
  , defaultPort
  , defaultIp
  , defaultDBIp
  , defaultDBName
  , defaultDBPasswd
  , defaultDBUser
  , defaultPGDBConnectStr
  , cmdLineOut
  ) where

import Options.Applicative

defaultPort :: Int
defaultPort = 8080

defaultIp :: String
defaultIp = "localhost"

defaultDBPort :: Int
defaultDBPort = 3306

defaultDBIp :: String
defaultDBIp = "0.0.0.0"

defaultDBName :: String
defaultDBName = "tezosid_block_id"

defaultDBPasswd :: String
defaultDBPasswd = "pw"

defaultDBUser :: String
defaultDBUser = "root"

defaultPGDBConnectStr :: String
defaultPGDBConnectStr = "host=0.0.0.0 dbname=tezos user=postgres port=5433"

data Arg =  Server
         { port           :: Maybe Int
         , ip             :: Maybe String
         , dbPort               :: Maybe Int
         , dbIp                 :: Maybe String
         , dbName               :: Maybe String
         , dbPasswd             :: Maybe String
         , dbUser               :: Maybe String
         , pgDBConnStr          :: Maybe String
         }

cmdLineOut :: Parser Arg
cmdLineOut = subparser
                ( command "Server"
                    ( info (helper <*> serverArg) fullDesc)
                <> help "Use '--help' to see more detail"
                )

serverArg :: Parser Arg
serverArg = Server
        <$> portParser
        <*> ipParser
        <*> dbPortParser
        <*> dbIpParser
        <*> dbNameParser
        <*> dbPasswdParser
        <*> dbUserParser
        <*> pgDBConnStrParser

portParser :: Parser (Maybe Int)
portParser = optional
              $ option auto
                   ( long ("default port: " <> show defaultIp)
                   <> short 'p'
                   <> metavar "PORT"
                   <> help ("default port: " <> show defaultIp)
                   )

ipParser :: Parser (Maybe String)
ipParser = optional
              $ strOption 
                   ( long ("default ip: " <> defaultIp)
                   <> short 'u'
                   <> metavar "IP"
                   <> help ("default ip: " <> defaultIp)
                   )

dbPortParser :: Parser (Maybe Int)
dbPortParser = optional
              $ option auto
                   ( long ("default db port: " <> show defaultDBPort)
                   <> short 'P'
                   <> metavar "DB_PORT"
                   <> help ("default db port: " <> show defaultDBPort)
                   )

dbIpParser :: Parser (Maybe String)
dbIpParser = optional
              $ strOption
                   ( long ("default db ip: " <> defaultDBIp)
                   <> short 'U'
                   <> metavar "DB_IP"
                   <> help ("default db ip: " <> defaultDBIp)
                   )

dbNameParser :: Parser (Maybe String)
dbNameParser = optional
              $ strOption
                   ( long ("default db name: " <> defaultDBName)
                   <> short 'N'
                   <> metavar "DB_NAME"
                   <> help ("default db name: " <> defaultDBName)
                   )

dbPasswdParser :: Parser (Maybe String)
dbPasswdParser = optional
              $ strOption
                   ( long ("default db password: " <> defaultDBPasswd)
                   <> short 'W'
                   <> metavar "DB_PASSWORD"
                   <> help ("default db password: " <> defaultDBPasswd)
                   )

dbUserParser :: Parser (Maybe String)
dbUserParser = optional
              $ strOption
                   ( long ("default db user: " <> defaultDBUser)
                   <> short 'S'
                   <> metavar "DB_PASSWORD"
                   <> help ("default db user: " <> defaultDBUser)
                   )

pgDBConnStrParser :: Parser (Maybe String)
pgDBConnStrParser = optional
                  $ strOption
                   ( long ("default pg db connection string: " <> defaultPGDBConnectStr)
                   <> short 'c'
                   <> metavar "PB_DB_CONNSTR"
                   <> help ("default pg db connection string: " <> defaultPGDBConnectStr)
                   )
