# Mooncake
---

The middleware of Tezos.ID

## Installation

The simple way is to use `stack`

```
stack build
stack install
```


## Usage

```
> mooncake Server -h
Usage: mooncake Server [-p|--default port: "localhost" PORT]
                  [-u|--default ip: localhost IP]
                  [-P|--default db port: 3306 DB_PORT]
                  [-U|--default db ip: 0.0.0.0 DB_IP]
                  [-N|--default db name: tezosid_block_id DB_NAME]
                  [-W|--default db password: pw DB_PASSWORD]
                  [-S|--default db user: root DB_PASSWORD]

Available options:
  -h,--help                Show this help text
  -p,--default port: "localhost" PORT
                           default port: "localhost"
  -u,--default ip: localhost IP
                           default ip: localhost
  -P,--default db port: 3306 DB_PORT
                           default db port: 3306
  -U,--default db ip: 0.0.0.0 DB_IP
                           default db ip: 0.0.0.0
  -N,--default db name: tezosid_block_id DB_NAME
                           default db name: tezosid_block_id
  -W,--default db password: pw DB_PASSWORD
                           default db password: pw
  -S,--default db user: root DB_PASSWORD
                           default db user: root

```

